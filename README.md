# Felice
An HTTP endpoint for Kafka

## Purpose
Felice provides a set of HTTP endpoints for sending data into and retrieving data from Kafka. The goal is allow you to quickly start writing applications that consume and produce Kafka messages without having to write HTTP endpoints and services yourself.

## Technical Overview
Felice is a Dropwizard application that can easily be deployed. Simply update the settings with you your cluster details and preferences and follow the deployment instrunctions. There is a Vagrantfile and a a set of Docker images that allow you to easily create a local cluster (ZooKeeper, Kafka, and Dropwizard) to test locally.

## Getting Started
There a few key files and directories:
* /Vagrantfile - The Vagrantfile for creating the local cluster for testing purposes.
* /src - the source of the Dropwizard application
* /images - the Dockerfiles used by the Vagrantfile to make the local cluster.

## Project Status / Contributing
This project is not currently suitable for production use. Keep an eye on the project issues. Pull requests are welcome!
