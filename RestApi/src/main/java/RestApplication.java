import io.dropwizard.Application;
import io.dropwizard.setup.Environment;
import org.apache.kafka.clients.producer.KafkaProducer;
import services.KafkaService;
import services.PersistenceService;

import java.util.Properties;

public class RestApplication extends Application<RestConfiguration> {

    public static void main(String[] args) throws Exception {
        new RestApplication().run(args);
    }

    @Override
    public void run(RestConfiguration restConfiguration, Environment environment) throws Exception {

        PersistenceService svc = new KafkaService();

        final TrackResource resource = new TrackResource(svc);

        environment.jersey().register(resource);
    }
}
