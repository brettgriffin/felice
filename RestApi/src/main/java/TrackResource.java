import com.codahale.metrics.annotation.Timed;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.joda.time.DateTime;
import services.PersistenceService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class TrackResource {

    PersistenceService persistenceService;


    public TrackResource(PersistenceService svc) {
        persistenceService = svc;
    }

    @GET
    @Timed
    @Path("t")
    public Response Track(
            @QueryParam("data") String data
    ) {
        try {
            System.out.println("/t track()");
            String decoded = URLDecoder.decode(data);
            System.out.println(decoded);

            Gson gson = new Gson();
            JsonObject obj = new JsonParser().parse(decoded).getAsJsonObject();

            // if the event does not have a unique identifier, generate a new one
            if (!obj.has("id")) {
                obj.addProperty("id",  UUID.randomUUID().toString());
            }

            long start = System.currentTimeMillis();

            obj.addProperty("ts", start);

            persistenceService.PersistMessage(obj.toString(), "analytics");
        } catch (Exception ex) {
            Response.status(500).build();
        }

        return Response.ok().build();
    }
}
