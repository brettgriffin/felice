package services;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.nio.charset.Charset;
import java.util.Properties;

public class KafkaService implements PersistenceService {

    private KafkaProducer producer;

    public KafkaService() {
        Properties props = new Properties();
        props.put("bootstrap.servers", "192.168.50.51:9092, 192.168.50.52:9092, 192.168.50.53:9092");
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.ByteArraySerializer");
        props.put("acks", "all");
        props.put("retries", 5);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        props.put("buffer.memory", 33554432);

        producer = new KafkaProducer(props);
    }

    public  void PersistMessage(String message, String topic) {
        Charset charset = Charset.forName("utf-8");
        ProducerRecord record = new ProducerRecord(topic, message.getBytes(charset));
        producer.send(record);
    }
}
