sudo apt-get update
sudo apt-get install -y ntp dstat screen git wget curl software-properties-common python-setuptools python-dev build-essential python-software-properties libffi-dev libssl-dev
sudo easy_install pip

#ufw
sudo ufw enable
sudo ufw default deny
sudo ufw allow 22

#docker
# wget -qO- https://get.docker.com/ | sh
# sudo usermod -aG docker $(whoami)

sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
sudo touch /etc/apt/sources.list.d/docker.list
sudo sh -c 'echo deb https://apt.dockerproject.org/repo ubuntu-trusty main > /etc/apt/sources.list.d/docker.list'
sudo apt-get update
sudo apt-get purge lxc-docker
sudo apt-cache policy docker-engine
sudo apt-get update
sudo apt-get install -y linux-image-extra-$(uname -r)
sudo apt-get update
sudo apt-get install -y docker-engine
sudo usermod -aG docker ubuntu
sudo usermod -aG docker vagrant

# Docker Compose
sudo pip install pyopenssl ndg-httpsclient pyasn1 docker-compose

# kafka
sudo ufw allow 32769
